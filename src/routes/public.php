<?php
// Create routes for public site

//Home
$router->map('GET', '/', 'home.php', 'home');
$router->map('GET', '/food', 'food.php', 'food');

//Log
$router->map('GET|POST', '/login', 'log/login.php', 'login');
$router->map('GET|POST', '/subscribe', 'log/subscribe.php', 'subscribe');

//Footer
$router->map('GET', '/about', 'footer/about.php', 'about');
$router->map('GET', '/condition', 'footer/condition.php', 'condition');
$router->map('GET', '/contact', 'footer/contact.php', 'contact');
