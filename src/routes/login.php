<?php
// Create routes for logged site

// Log
$router->map('GET', '/logout', 'log/logout.php', 'logout');
$router->map('GET', '/profile', 'log/profile.php', 'profile');

//Recipes
$router->map('GET', '/myRecipes', 'log/recipes/myRecipes.php', 'myRecipes');
$router->map('GET|POST', '/recipeForm', 'log/recipes/recipeForm.php', 'recipeForm');
$router->map('GET', '/recipe', 'log/recipes/recipe.php', 'recipe');