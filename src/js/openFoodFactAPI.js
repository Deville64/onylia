'use strict'

//When the user clicks on the button, 
//toggle between hiding and showing the dropdown content
function showHideDropDown() {
    document.getElementById("dropDown").classList.toggle("show");
}

// Close the dropdown for search bar the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('#search')) {
        const dropdown = document.getElementById("dropDown");
        dropdown.classList.remove("show");
    }
}

function searchFood() {
    const xhr = new XMLHttpRequest();     // Creating the XMLHttpRequest object
    const text = document.getElementById("search").value;     //Take text from search bar
    const url = "https://fr.openfoodfacts.org/cgi/search.pl?search_simple=1&action=process&json=true&search_terms=" + text;

    //Check there is at least 3 characters to search food
    if (text.length >= 3) {
        xhr.open('GET', url);       // Instantiating the request object
        xhr.onload = function () {
            const openFoodFactDb = JSON.parse(xhr.responseText);
            const listElem = document.getElementById("dropDown");

            //Purge old results
            while (listElem.firstChild) {
                listElem.removeChild(listElem.firstChild)
            }

            for (let info = 0; info < 4; info++) {
                const foodName = openFoodFactDb.products[info].product_name_fr;
                const picFood = openFoodFactDb.products[info].image_front_small_url;
                const nutriscore = openFoodFactDb.products[info].nutriscore_grade;
                const imgNutriscore = "https://static.openfoodfacts.org/images/misc/nutriscore-" + nutriscore + ".svg"
                const nova = openFoodFactDb.products[info].nova_groups;
                const imgNova = "https://static.openfoodfacts.org/images/misc/nova-group-" + nova + ".svg";
                const foodID = openFoodFactDb.products[info].id;

                const node = document.createElement("div");
                node.foodID = foodID;
                node.className = "resultFoodSearch";

                //When clicked, save ID and send to food page
                node.addEventListener("click", function (event) {
                    console.log(event.target.foodID);
                    sessionStorage.setItem("foodID", event.target.foodID); // keep foodID in memory
                    location.assign('../onylia/food');
                });

                //Food Picture
                if (picFood != undefined) {
                    const createPicFood = document.createElement('img');
                    createPicFood.setAttribute('src', picFood);
                    createPicFood.className = "foodPicture";
                    createPicFood.foodID = foodID;
                    node.appendChild(createPicFood);
                }

                //Food Name
                if (foodName != undefined) {
                    const spanFoodName = document.createElement("span");
                    const foodNameNode = document.createTextNode(foodName);
                    spanFoodName.className = "foodName";
                    spanFoodName.foodID = foodID;
                    spanFoodName.appendChild(foodNameNode);
                    node.appendChild(spanFoodName);
                }

                //Nutriscore Image
                if (nutriscore != undefined) {
                    const createImgNutriscore = document.createElement("img");
                    createImgNutriscore.setAttribute('src', imgNutriscore);
                    createImgNutriscore.className = "nutriscore";
                    createImgNutriscore.foodID = foodID;
                    node.appendChild(createImgNutriscore);
                }

                //Nova Image
                if (nova != undefined) {
                    const createImgNova = document.createElement("img");
                    createImgNova.setAttribute('src', imgNova);
                    createImgNova.className = "nova";
                    createImgNova.foodID = foodID;
                    node.appendChild(createImgNova);
                }
                listElem.appendChild(node);
            }
        }
        xhr.send(); // Sending the request to the server
        console.log('Request Succesfull')
    }
}

//Use the food ID in local Storage to show main info in food page
function showOverview() {
    const foodID = sessionStorage.getItem('foodID');  // get back foodID from the food search
    const xhr = new XMLHttpRequest();     // interact with server
    const url = "https://world.openfoodfacts.org/api/v0/product/" + foodID + ".json";      //url to go to JSON file
    console.log(foodID)
    xhr.open('GET', url);

    xhr.onload = function () {
        const openFoodFactDb = JSON.parse(xhr.responseText);
        const product = openFoodFactDb.product;

        const foodName = product.product_name_fr;
        const picFood = product.image_front_url;
        const nutriscore = product.nutriscore_grade;
        const imgNutriscore = "https://static.openfoodfacts.org/images/misc/nutriscore-" + nutriscore + ".svg";
        const nova = product.nova_groups;
        const imgNova = "https://static.openfoodfacts.org/images/misc/nova-group-" + nova + ".svg";
        const ingredients = product.ingredients_text;
        const fat = product.nutriments.fat_100g;
        const saturatedFat = product.nutriscore_data.saturated_fat_value;
        const sugar = product.nutriments.sugars_100g;
        const salt = product.nutriments.salt_100g;

        //Go through all additives from API
        for (let index = 0; index < product.additives_original_tags.length; index++) {
            const list = document.getElementById("additives");
            const li = document.createElement("li");

            const additives = product.additives_original_tags[index];

            const additivesNode = document.createTextNode(additives.substring(3));
            li.appendChild(additivesNode);
            list.appendChild(li);
        };

        if (nutriscore != undefined) {
            document.getElementById("imgNutriscore").src = imgNutriscore;
        };
        if (nova != undefined) {
            document.getElementById("imgNova").src = imgNova;
        };
        document.getElementById("foodName").innerText = foodName;
        document.getElementById("picFood").src = picFood;
        document.getElementById("fat").innerText = fat;
        document.getElementById("saturatedFat").innerText = saturatedFat;
        document.getElementById("sugar").innerText = sugar;
        document.getElementById("salt").innerText = salt;
        document.getElementById("ingredients").innerText = ingredients;
    };
    xhr.send(); //Sends request to server
};