'use strict'

//Show hints depending on what is in ingredient input
function showHint(text, id) {
    if (text.length == 0) {
        document.getElementById(id).innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            document.getElementById(id).innerHTML = this.responseText;
        };
        xmlhttp.open("GET", "./src/includes/gethint.php?q=" + text, true);
        xmlhttp.send();
    }
};

function selectHint(event, id) {
    let getHint = event.target;

    let getInputID = getHint.parentNode.id.replace("dropdown", '');
    let giveInputName = document.getElementById("ingredient" + getInputID);
    let giveInputId = document.getElementById("ingredientId" + getInputID);

    giveInputName.value = getHint.innerHTML;
    giveInputId.value = id;
};

//Create Ingredient, Quantity Inputs if click on add ingredient
let index = 0;
function createIngredient() {
    index++;
    const getUl = document.getElementById("ingredients");

    const li = document.createElement('li');
    li.className = "ingredients";

    const ingredient = document.createElement('input');
    ingredient.placeholder = "Selectionner un ingrédient";
    ingredient.id = "ingredient" + index;
    ingredient.className = "ingredient";
    ingredient.autocomplete = "off";

    const div = document.createElement('div');
    div.id = "dropdown" + index;

    const ingredientId = document.createElement('input');
    ingredientId.id = "ingredientId" + index;
    ingredientId.style.display = "none"; 
    ingredientId.name = "ingredients[ingredientsId][]";


    const quantity = document.createElement('input');
    quantity.placeholder = "Choisir une quantité";
    quantity.id = "quantity" + index;
    quantity.className = "quantity";
    quantity.name = "ingredients[quantity][]";
    quantity.autocomplete = "off";

    getUl.appendChild(li);
    li.appendChild(ingredient);
    li.appendChild(ingredientId);
    li.appendChild(div);
    li.appendChild(quantity);

    return document.getElementById("ingredient" + index).onkeyup = function () { showHint(this.value, div.id) };
};