<?php
// Debug mode
define('DEBUG', true);

// Database informations
define('DB_HOST', 'localhost');
define('DB_NAME', 'onylia');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

// Path
define('BASEPATH', '/greta/onylia');
define('ROOT', dirname(__DIR__));
define('CONTROLLERS', ROOT . '/controllers/');
define('PAGES', ROOT . '/views/pages/');
define('ASSETS', BASEPATH . '/src/assets/');