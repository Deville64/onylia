<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo ASSETS ?>css/main.css">
    <title>Onylia</title>
</head>

<body>
    <header id="sticky">
        <div class="wrapper">
            <a id="logo" href="<?php echo $router->generate('home'); ?>">Onylia</a>

            <nav id="menu">
                <form action="" method="get">
                    <label for="search">Recherche</label>
                    <input id="search" type="text" placeholder="Rechercher une marque" autocomplete="off" onkeyup="searchFood()" onclick="showHideDropDown()">
                    <div id="dropDown" class="dropdown-content"></div>
                </form>


                <ul id="menuLogin">
                    <li class="liMenuLogin"><a href="<?php echo $router->generate('myRecipes'); ?>">Mes recettes</a></li>
                    <li class="liMenuLogin">
                        <label for="showMenuLogin" id="icon"><img src="./src/assets/images/general/IconProfil.png" alt=""></label>
                        <input type="checkbox" id="showMenuLogin" role="button">
                        <ul id="iconMenuLogin">
                            <li><a href="<?php echo $router->generate('myRecipes'); ?>">Mes recettes</a></li>
                            <li><a href="<?php echo $router->generate('profile'); ?>">Profil</a></li>
                            <li><a href="<?php echo $router->generate('logout'); ?>">Deconnexion</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </header>