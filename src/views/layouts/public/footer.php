<footer>
    <div class="wrapper">
        <nav>
            <ul>
                <li><a href="<?php echo $router->generate('about'); ?>">A Propos</a></li>
                <li><a href="<?php echo $router->generate('contact'); ?>">Contact</a></li>
                <li><a href="<?php echo $router->generate('condition'); ?>">Conditions d'utilisation</a></li>
            </ul>
        </nav>

        <p id="right">© 2020 Onylia</p>
    </div>
</footer>
</body>
</html>
<script src="./src/js/checkScreen.js"></script>
<script src="./src/js/openFoodFactAPI.js"></script>