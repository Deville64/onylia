<?php get_header($state); ?>

<main id="food" class="wrapper">

    <div id="Wrap">
        <h1 id="foodName"></h1>
        <img id="picFood" src="" alt="">

        <h2>Informations nutritionnelles</h2>
        <img id="imgNutriscore" src="" alt="">

        <h2>Repères nutritionnels pour 100 g</h2>
        <ul>
            <li><span id="fat"></span>g</li>
            <li><span id="saturatedFat"></span>g</li>
            <li><span id="sugar"></span>g</li>
            <li ><span id="salt"></span>g</li>
        </ul>
    </div>

    <div id="wrap1">
        <h2>Ingrédients:</h2>
        <p id="ingredients"></p>

        <h2>Additifs:</h2>
        <ul id="additives"></ul>

        <h2>Groupe Nova</h2>
        <img id="imgNova" src="" alt="">

        <input type="button" value="Ajouter à ma recette">
    </div>

</main>

<?php get_footer($state); ?>
<script>showOverview()</script>