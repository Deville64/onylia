<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo ASSETS ?>css/main.css">
    <title>Onylia</title>
</head>

<body id="subscribe" class="wrapper">
    <a id="logo" href="<?= $router->generate('home'); ?>">Onylia</a>

    <h1>Inscription</h1>

    <form action="" method="post">
        <div class="group">
            <input type="email" name="email" required>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label for="email">Adresse email</label>
        </div>

        <div class="group">
            <input type="password" name="password" required>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label for="password">Mot de passe</label>
        </div>

        <input type="submit" value="S'inscrire">
    </form>

    <div id="other">
        <p>Vous êtes déjà inscrit ? <a href="<?= $router->generate('login'); ?>">Connectez-vous</a></p>
    </div>
</body>
</html>