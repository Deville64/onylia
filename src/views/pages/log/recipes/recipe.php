<?php get_header($state); ?>

<main id="recipe" class="wrapper">

    <h1 id="name"></h1>
    <img id="pictureName" src="" alt="">
    <ul>
        <li>
            <img src="" alt="">
            <ul>
                <li>
                    <h3>Préparation</h3>
                </li>
                <li id="preparationTime"></li>
            </ul>
        </li>
        <li>
            <img src="" alt="">
            <ul>
                <li>
                    <h3>Cuisson</h3>
                </li>
                <li id="cookingTime"></li>
            </ul>
        </li>
    </ul>

    <h2>Ingrédients</h2>
    <ul>
    </ul>

    <h2>Préparation</h2>
    <p id="description"></p>

</main>

<?php get_footer($state); ?>