<?php get_header($state); ?>

<main id="myRecipes" class="wrapper">
    <h1>Mes recettes</h1>
    <ul id="recipes">
        <?php foreach ($result as $value) { ?>
            <li>
                <h2><?php echo $value['recipeName']; ?></h2>
            </li>
        <?php } ?>
        <li> <a href="<?php echo $router->generate('recipeForm'); ?>"><img id="addMore" src="./src/assets/images/general/addMore.png" alt=""></a></li>
    </ul>
</main>

<?php get_footer($state); ?>