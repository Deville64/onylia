<?php get_header($state); ?>

<main id="recipeForm" class="wrapper">

    <h1>Creation de ma recette</h1>

    <form action="" method="post">
        <input id="recipeName" name="recipeName" type="text" placeholder="Nom de ma recette" autocomplete="off">

        <ul>
            <li><input name="preparationTime" type="text" placeholder="Temps de préparation" autocomplete="off"></li>
            <li><input name="cookingTime" type="text" placeholder="Temps de cuisson" autocomplete="off"></li>
        </ul>

        <h2>Ingrédients:</h2>
        <ul id="ingredients">
            <li><img src="./src/assets/images/general/addMore.png" alt="" id="moreIngredients" onclick="createIngredient()">
                <p>Ajouter un ingrédient</p>
            </li>
        </ul>

        <h2>Préparation:</h2>
        <textarea name="preparation" id="preparation" cols="30" rows="10" autocomplete="off"></textarea>

        <input type="submit" value="Valider">
    </form>
</main>

<?php get_footer($state); ?>
<script src="./src/js/ingredients.js"></script>