<?php get_header($state); ?>
<main id="profile">
    <?php get_menu('profile'); ?>

    <?php foreach ($result as $value) { ?>
        <section id="personalInfo" class="hide" style="display: inline-block;">
            <h1><span>Information personnelles</span></h1>

            <form action="" method="post">
                <div>
                    <label for="email">Nom:</label>
                    <input type="email" name="email" placeholder="Entrez votre email" value="<?php echo $value['email']; ?>">
                </div>

                <div class="button">
                    <input type="submit" value="Enregistrer">
                </div>

            </form>
        </section>
    <?php } ?>

    <section id="picture" class="hide">
        <h1><span>Photo</span></h1>

        <form action="" method="post">
            <div>
                <label for="address">Adresse:</label>
                <input type="text" name="address" placeholder="Ex: 12 rue des bois">
            </div>

            <div class="button">
                <input type="submit" value="Enregistrer">
            </div>
        </form>
    </section>
</main>

<?php get_footer($state); ?>
<script src="./src/js/showHide.js"></script>