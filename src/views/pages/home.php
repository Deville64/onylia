<?php get_header($state); ?>

<main id="home" class="wrapper">

    <div id="banner">
        <button onclick="slide()" id=previous-btn><</button>
        <button onclick="backSlide()" id=next-btn>></button>
        <img id="home-banner">
    </div>


<h1>Un projet 100% indépendant</h1>

<ul>
    <li>
        <img src="./src/assets/images/home/noInfluence.PNG" alt="">
        <p>Aucune marque ni groupe industriel ne peut influencer la notation et les recommandations proposées.</p>

    </li>
    <li>
        <img src="./src/assets/images/home/noAdd.PNG" alt="">
        <p>Aucune marque ne peut nous rémunérer pour faire la promotion de ses produits dans l'application.</p>
    </li>
</ul>

</main>

<?php get_footer($state); ?>
<script src="./src/js/slideShow.js"></script>