<?php

function addUser() {
	global $db;
	$data = [
		'email' => $_POST['email'],
		'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
	];
	$sql = 'INSERT INTO users (email, password) VALUES (:email, :password)';
	$request = $db->prepare($sql);
	$request->execute($data);
	unset($_POST);
	die();
}

if ($_POST) {
	addUser();
	header('Location: ' . $router->generate('home'));
	die();
}