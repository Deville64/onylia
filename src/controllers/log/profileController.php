<?php
//Used to Show data in HTML page
$sql = 'SELECT * FROM users WHERE id =' . $_SESSION['id'];
$request = $db->query($sql);
$result = $request->fetchALL();

//Edit user
function updateUser($router){
    global $db;
    $data = [
        'email' => $_POST['email']
    ];

    $sql = 'UPDATE users SET email = :email, modifiedAt = NOW() WHERE id =' . $_SESSION['id'];
    $request = $db->prepare($sql);
    $request->execute($data);
    header('Location: ' . $router->generate('profile'));
    die();
}

if ($_POST) {
    updateUser($router);
}