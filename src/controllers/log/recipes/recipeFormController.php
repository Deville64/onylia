<?php
function createRecipe($router)
{
    global $db;
    $recipeData = [
        'recipeName' => $_POST['recipeName'],
        'preparationTime' => $_POST['preparationTime'],
        'cookingTime' => $_POST['cookingTime'],
        'preparation' => $_POST['preparation'],
        'userId' => $_SESSION['id']
    ];

    $sqlRecipe = 'INSERT INTO recipes (recipeName, preparationTime, cookingTime, preparation, userId) VALUES (:recipeName, :preparationTime, :cookingTime, :preparation, :userId)';
    $request = $db->prepare($sqlRecipe);
    $request->execute($recipeData);
    $recepeId = $db->lastInsertId();

    $data = array();
    for ($i = 0; $i < count($_POST['ingredients']['quantity']); $i++) {
        $data[] = '("' . $recepeId . '","' . $_POST['ingredients']['ingredientsId'][$i] . '","' . $_POST['ingredients']['quantity'][$i] . '")';
    }

    $sql = "INSERT INTO recipes_ingredients (recipesID, ingredientsId, quantity) VALUES " . implode(',', $data);
    $request = $db->query($sql);
    unset($_POST);
    
    header('Location: ' . $router->generate('myRecipes'));
    die();
};

if ($_POST) {
    createRecipe($router);
    die();
};
